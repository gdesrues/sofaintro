# Introduction to Sofa

- [Installation of Sofa and plugins](https://gitlab.inria.fr/gdesrues1/sofaintro/tree/master/Install)
- [First scene (python)](https://gitlab.inria.fr/gdesrues1/sofaintro/tree/master/Python_Examples)
- [Main principles of the API (C++)](https://gitlab.inria.fr/gdesrues1/sofaintro/tree/master/Plugin_Example)


### Content of my nef `.bashrc`
```bash
runSofa='/home/gdesrues/sofa/v20.12/build/bin/runSofa'
alias runSofa=$runSofa


# For pyvista
export DISPLAY=:99.0;
export PYVISTA_OFF_SCREEN=true;
export PYVISTA_USE_PANEL=true;
Xvfb :99 -screen 0 1024x768x24 > /dev/null 2>&1 &
sleep 3;
export MESA_GL_VERSION_OVERRIDE=3.2;
export XDG_RUNTIME_DIR=/tmp/runtime-gdesrues;


# Use Scene launcher for 3D electromechanical simulation
export CARDIAC_CLUSTER=1
export CARDIAC_SOFA_PATH=$runSofa
export CARDIAC_BASE_SUBJECT_PATH="path-to-data"


# Load modules on nef-devel
SOURCE_MODULES=/etc/profile.d/modules.sh
MODULES=("conda/2020.11-python3.8" "cmake/3.19.6" "boost/1.58.0" "gcc/9.2.0")

if test -f "$SOURCE_MODULES"; then
   source "$SOURCE_MODULES"
   for x in "${MODULES[@]}"; do
      echo "Loading module $x"
      module load $x
   done
fi
```


### Content of local `.bashrc`
```bash
alias vpnconnect="/opt/cisco/anyconnect/bin/vpn connect vpn.inria.fr"
alias vpndisconnect="/opt/cisco/anyconnect/bin/vpn disconnect"
alias vpnstatus="/opt/cisco/anyconnect/bin/vpn status"

export CARDIAC_CLUSTER='0'
export CARDIAC_SOFA_PATH='/media/gaetan/UbuntuHDD/sofa/build/v20.12/bin/runSofa'
export CARDIAC_BASE_SUBJECT_PATH='path-to-data'
```
