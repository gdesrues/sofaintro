# Add a custom plugin

## Build your plugin
- Add the `EmptyPlugin` dir to your `SOFA_EXTERNAL_DIRECTORIES` folder.
- Add the following line to your `SOFA_EXTERNAL_DIRECTORIES`'s '`CMakeLists.txt`:
  ```
  sofa_add_plugin(EmptyPlugin EmptyPlugin)
  ```
- Run CMake
- Enable the compilation of `EmptyPlugin`, then configure
- Build the project


## Test it

*Tip: Add the path of runSofa to your bash_profile: `alias runSofa='<path-to-your-sofa-build>/bin/runSofa'`*

In the scene, you will find the only component (for the moment) of your plugin:
```
Bar.createObject('EmptyComponent', name="MyName", emptyData="The data's value")
```

Launch the scene with either

    runSofa scene.py
    runSofa scene.py -g batch -n 1000


You can see in the shell output:
```
[WARNING] [EmptyComponent(MyName)] In EmptyComponent::init
My data emptyData is equal to The data's value
```

which is the proof that your plugin is valid.

You can add other components to your plugin following the same process.
