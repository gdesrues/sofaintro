#include "EmptyComponent.h"
#include <sofa/core/ObjectFactory.h>

namespace sofa
{

namespace component
{

namespace engine
{

EmptyComponent::EmptyComponent()
    : m_emptyData(initData(&m_emptyData, "emptyData", "description of the data"))
{
}


EmptyComponent::~EmptyComponent()
{
}


void EmptyComponent::init()
{
    msg_warning() << "In " << SOFA_CLASS_METHOD << "\nMy data " << m_emptyData.getName() << " is equal to " << m_emptyData.getValue();
}


void EmptyComponent::doUpdate()
{
}


void EmptyComponent::draw(const core::visual::VisualParams* /*vparams*/)
{
}



int EmptyComponentClass = sofa::core::RegisterObject("This class does nothing").add<EmptyComponent>();


} // engine

} // namespace component

} // namespace sofa
