#ifndef EMPTYPLUGIN_EMPTYCOMPONENT_H
#define EMPTYPLUGIN_EMPTYCOMPONENT_H

#include "config.h"
#include <sofa/core/DataEngine.h>

namespace sofa
{

namespace component
{

namespace engine
{

class EmptyComponent : public sofa::core::DataEngine
{
public:
    SOFA_CLASS(EmptyComponent, sofa::core::DataEngine);

    EmptyComponent();
    ~EmptyComponent() override;
    void init() override;
    void doUpdate() override;
    void draw(const core::visual::VisualParams*) override;

private:
    Data<std::string> m_emptyData;

};

} // namespace engine

} // namespace component

} // namespace sofa

#endif
