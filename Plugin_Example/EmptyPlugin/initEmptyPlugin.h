#ifndef INIT_EMPTYPLUGIN_H
#define INIT_EMPTYPLUGIN_H

#include "config.h"

#ifdef SOFA_BUILD_EMPTYPLUGINPLUGIN
#define SOFA_EMPTYPLUGIN_API SOFA_EXPORT_DYNAMIC_LIBRARY
#else
#define SOFA_EMPTYPLUGIN_API SOFA_IMPORT_DYNAMIC_LIBRARY
#endif

/** mainpage
This is the main page of the doxygen documentation for MyPlugin.
 */

#endif
