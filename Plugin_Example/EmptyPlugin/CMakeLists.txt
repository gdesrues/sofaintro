cmake_minimum_required(VERSION 3.1)

project(EmptyPlugin VERSION 0.1)

#look for all packages on which your project depends
find_package(SofaFramework REQUIRED)

set(HEADER_FILES
    initEmptyPlugin.cpp
    EmptyComponent.h
    config.h
)

set(SOURCE_FILES
    initEmptyPlugin.h
    EmptyComponent.cpp
)

add_library(${PROJECT_NAME} SHARED ${HEADER_FILES} ${SOURCE_FILES})
target_link_libraries(${PROJECT_NAME} SofaCore)
set_target_properties(${PROJECT_NAME} PROPERTIES COMPILE_FLAGS "-DSOFA_BUILD_EMPTYPLUGIN")

install(TARGETS EmptyPlugin
        RUNTIME DESTINATION bin
        LIBRARY DESTINATION lib
        ARCHIVE DESTINATION lib)
