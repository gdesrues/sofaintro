#include "initEmptyPlugin.h"

extern "C" {
    void initExternalModule()
    {
        // Here is the place to write initialisation code, that will be executed
        // before any component is created.
    }

    const char* getModuleName()
    {
        return "EmptyPlugin";
    }

    const char* getModuleVersion()
    {
        return "0.1";
    }

    const char* getModuleLicense()
    {
        return "LGPL";
    }

    const char* getModuleDescription()
    {
        return "Plugin description";
    }

    const char* getModuleComponentList()
    {
        return "EmptyComponent";
    }
}
