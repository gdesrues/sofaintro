# Table of contents

This document is supposed to be complementary to the [sofa documentation](https://www.sofa-framework.org/community/doc/getting-started/build/linux/).

- [Install Sofa from sources and plugins on Linux](#install-sofa-from-sources-and-plugins-on-linux)
  + [Download the prerequisites for linux](#download-the-prerequisites-for-linux)
  + [Download Sofa](#download-sofa)
  + [Build Sofa](#build-sofa)
  + [Add a plugin](#add-a-plugin)
  + [Other versions of dependancies](#other-versions-of-dependancies)
    * [GCC 7.3.0](#gcc-7.3.0)
    * [CMake 3.15.0](#cmake-3.15.0)
  + [Test SOFA](#test-sofa)
- [On the cluster](#on-the-cluster)
  + [Build Sofa](#build-sofa)
  + [Test Sofa](#test-sofa)

# Install Sofa from sources and plugins on Linux


Seems to be sufficient (fresh install fedora 34):
Install Qt with [the installer](https://github.com/Kitware/CMake/releases/latest).

```bash
sudo dnf install eigen3-devel-3.3.9-4.fc34.noarch
sudo dnf install glew-devel

cmake .. \
-DCMAKE_PREFIX_PATH=<path-to>/Qt/5.15.2/gcc_64 \
-DPYTHON_INCLUDE_DIR=$(python -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())")  \
-DPYTHON_LIBRARY=$(python -c "import distutils.sysconfig as sysconfig; print(sysconfig.get_config_var('LIBDIR'))") \
../src
```


## Download the prerequisites for linux
- As mentioned in the documentation, install the standard compilation toolkit

```
sudo apt-get install build-essential
```

- CMake :

```
sudo apt-get install cmake cmake-qt-gui
```


- Boost :

```
sudo apt-get install libboost-atomic-dev libboost-chrono-dev libboost-date-time-dev libboost-filesystem-dev libboost-locale-dev libboost-regex-dev libboost-system-dev libboost-thread-dev libboost-program-options-dev
```


- Additional libraries :

```
sudo apt-get install libpng-dev libjpeg-dev libtiff-dev zlib1g-dev libglew-dev
```


- Qt : go to https://www.qt.io/download and select "Go open source". After download, open a terminal and type

```
chmod +x qt-unified-linux-x64-3.1.1-online.run
./qt-unified-linux-x64-3.1.1-online.run
```

While selecting components, the only component you need to install is the kit "Desktop gcc 64-bit", or equivalent
![component_qt](images/qt_comp.png)



## Download Sofa
Note that you can download the binaries of Sofa [here](https://www.sofa-framework.org/download/).
For the sources, go to https://github.com/sofa-framework/sofa/releases and pick the suitable version, then download the sources or clone the repo.


You can follow the proposed hierarchy for your sofa directories :
- sofa/
  - external_plugins/
  - src/
    - v19.06/
  - build/
    - v19.06/


## Build Sofa
You can now open qt-creator. Go to the build parameters (tool on the left or ctrl+5).
- You can select your kit on the left, under "Build & Run" and modify then by clicking on "Manage Kits..."
- You can modify the cmake build settings by enabling/disabling options, hitting enter and then configure


Build the project clicking on the hammer on the left or ctrl+B. You can view the progression in the "Compile Output" panel (alt+4).

You can build specific plugins by enabling them in the build settings of Qt or in the terminal with CMake. For the SofaEpione plugin, enable `SofaEpione`, Configure, enable `CardiacMeshTools`, `Electrophysiology` and `MechanicalHeart` then Configure again. You may want to enable `SofaPython` as well.


See the [possible issues](https://gitlab.inria.fr/gdesrues1/sofaintro/issues?scope=all&utf8=%E2%9C%93&state=all).



## Add a plugin

Building a plugin is well explained in the [sofa doc](https://www.sofa-framework.org/community/doc/using-sofa/build-a-plugin/).

You would eventually add the sources of your plugin to the "external-plugins" directory. Then `Apply Configuration Changes`.
![Add the SOFA_EXTERNAL_DIRECTORIES macro](images/external.png)


In order to be understood by cmake, add a "CMakeLists.txt" to this directory :

    cmake_minimum_required(VERSION 3.1)
    find_package(SofaFramework)
    sofa_add_plugin(plugin1/ name_of_plugin1)


To build the plugin, you need to enable the option in the cmake settings, and then `Apply Configuration Changes`.
![Enable the build of the plugin](images/flexible.png)


To create a plugin, see the [doc](https://www.sofa-framework.org/community/doc/programming-with-sofa/start-coding/create-your-plugin/).








## Other versions of dependancies



### GCC 7.3.0
    wget https://ftp.gnu.org/gnu/gcc/gcc-7.3.0/gcc-7.3.0.tar.gz
    tar -zxvf gcc-7.3.0.tar.gz
    cd gcc-7.3.0
    ./contrib/download_prerequisites
    cd ..
    mkdir objdir
    cd objdir
    ../gcc-7.3.0/configure --prefix=$HOME/opt/gcc/7.3.0 --enable-languages=c,c++ --disable-multilib
    make -j 12
    make install -j 12




    make
    make install





### CMake 3.15.0
    wget https://github.com/Kitware/CMake/releases/download/v3.15.0-rc4/cmake-3.15.0-rc4.tar.gz
    tar -zxvf cmake-3.15.0-rc4.tar.gz
    cd cmake-3.15.0-rc4

<!-- Version minimum de gcc : 4.8.1... -->





## Test SOFA

```
git clone git@gitlab.inria.fr:gdesrues1/sofaexample.git
cd sofaexample
runSofa scene.py
```

You should see the following window :
![Test ok](images/testScene.png)










# On the cluster
## Build Sofa

On the Inria cluster (connect with `ssh login@nef-devel.inria.fr` or `ssh nef-devel` from local network), there are some dependancies already installed :
- gcc 7.3.0
- cmake 3.5.2
- boost 1.58.0

You must load the modules before using them. You can add the following lines to your `bashrc`:
```
module load gcc/7.3.0
module load boost/1.58.0
module load cmake/cmake-3.5.2
```

Go to the [download page](https://www.sofa-framework.org/download/) of sofa and clone the project from github, for example with version 19.06 :
```
git clone -b v19.06 https://github.com/sofa-framework/sofa.git sofa/v19.06/src/
cd sofa/v19.06
```

Create a build folder and generate build files with cmake :
```
mkdir build && cd build
ccmake ../src
```

You can now choose your parameters and customize your version of sofa. Then configure **twice** with `c` and generate with `g`.

Build sofa with
```
make -j12
```

See the [possible issues](https://gitlab.inria.fr/gdesrues1/sofaintro/issues?scope=all&utf8=%E2%9C%93&state=all).


## Test Sofa

Start a job with oarsub (see https://wiki.inria.fr/ClustersSophia/FAQ_new_config)

    git clone git@gitlab.inria.fr:gdesrues1/sofaexample.git
    cd sofaexample
    oarsub "<path-to-sofa-build>/bin/runSofa scene.py"

See the job with `oarstat -u <username>`

You can use `sshfs` to view the results on your local machine:

    mkdir nef-sofaintro && cd nef-sofaintro
    sshfs nef-devel:/home/<username>/sofaintro .
    paraview deformed.vtk


You can use VNCViewer:

    vncviewer -via nef-devel.inria.fr vnc-<username>:0
