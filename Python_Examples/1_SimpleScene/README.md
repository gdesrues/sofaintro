# Example simple scene

*Tip: Add the path of runSofa to your bash_profile: `alias runSofa='<path-to-your-sofa-build>/bin/runSofa'`*

Launch the scene with either

    runSofa scene.py
    runSofa scene.py -g batch -n 1000



It will generate a `deformed.vtk` file to open with paraview.

    # Graph of scene.py:
    rootNode
         VisualStyle
         DefaultAnimationLoop
         DefaultVisualManagerLoop
         RequiredPlugin
         RequiredPlugin
         EulerImplicitSolver
         CGLinearSolver
         -> Bar
              MeshVTKLoader
              Mesh
              MechanicalObject
              TetrahedronFEMForceField
              UniformMass
              EmptyComponent
              VTKExporter
              -> DirichletBoundary
                   BoxROI
                   RestShapeSpringsForceField
              -> PressureBoundary
                   TriangleSetTopologyContainer
                   TriangleSetTopologyModifier
                   TriangleSetTopologyAlgorithms
                   TriangleSetGeometryAlgorithms
                   BoxROI
                   TrianglePressureForceField
              -> visual
                   MeshSTLLoader
                   OglModel
                   BarycentricMapping
