import Sofa
import os

class Scene(Sofa.PythonScriptController):
    def __init__(self, node):
        self.createGraph(node)
        return None


    def createGraph(self, rootNode):
        # rootNode
        self.rootNode = rootNode
        rootNode.dt = 0.1
        rootNode.gravity = "0 0 0"
        rootNode.createObject('VisualStyle', displayFlags='showVisual showWireframe showForceFields showBehavior')
        rootNode.createObject('DefaultAnimationLoop')
        rootNode.createObject('DefaultVisualManagerLoop')
        rootNode.createObject("RequiredPlugin", pluginName="SofaOpenglVisual SofaExporter SofaPython")
        rootNode.createObject('EulerImplicitSolver', rayleighStiffness='0.1', rayleighMass='0.1')
        rootNode.createObject('CGLinearSolver', iterations='100')


        # rootNode/Bar
        Bar = rootNode.createChild('Bar')
        self.Bar = Bar
        Bar.createObject('MeshVTKLoader', name='loader', filename='../mesh/undeformed.vtk') # 10m x 1m x 1m
        Bar.createObject('Mesh', name="mesh", src="@loader")

        Bar.createObject('MechanicalObject', name='dof', template="Vec3d")
        Bar.createObject('TetrahedronFEMForceField', poissonRatio=0.3, youngModulus=15e5) # E = 200 GPa (Steel)
        Bar.createObject('UniformMass', totalMass=1e5)

        Bar.createObject('VTKExporter', filename="deformed.vtk", XMLformat=0, edges=0, triangles=0, tetras=1,
                        exportAtEnd=1, position="@dof.position", overwrite=1)


        # rootNode/Bar/DirichletBoundary
        DirichletBoundary = Bar.createChild('DirichletBoundary')
        self.DirichletBoundary = DirichletBoundary
        DirichletBoundary.createObject('BoxROI', box='0 0 0 0 1 1', position='@../mesh.position', template='Vec3d', drawSize="10", drawPoints="1")
        DirichletBoundary.createObject('RestShapeSpringsForceField', points='@[-1].indices', template='Vec3d', stiffness='1E10')


        # rootNode/Bar/PressureBoundary
        PressureBoundary = Bar.createChild('PressureBoundary')
        self.PressureBoundary = PressureBoundary
        PressureBoundary.createObject('TriangleSetTopologyContainer', triangles="@../loader.triangles")
        PressureBoundary.createObject('TriangleSetTopologyModifier')
        PressureBoundary.createObject('TriangleSetTopologyAlgorithms')
        PressureBoundary.createObject('TriangleSetGeometryAlgorithms')
        PressureBoundary.createObject('BoxROI', box='0 0 1 10 1 1', position='@../mesh.position', template='Vec3d', drawSize="5",
                                    drawTriangles='1', triangles="@../loader.triangles")
        PressureBoundary.createObject('TrianglePressureForceField', name="Pressure", triangleList="@[-1].triangleIndices",
                                    pressure="0 0 -200")


        # rootNode/Bar/visual
        visual = Bar.createChild('visual')
        self.visual = visual
        visual.createObject('MeshSTLLoader', name='stlloader', filename='../mesh/undeformed.stl')
        visual.createObject('OglModel', name='Visual', template='ExtVec3d', src='@stlloader', color='gray')
        visual.createObject('BarycentricMapping', template='Vec3d,ExtVec3d')

        return 0


def createScene(rootNode):
    myScene = Scene(rootNode)
    return 0
