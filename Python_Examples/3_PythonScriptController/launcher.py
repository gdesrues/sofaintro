from sofatools import Launcher
import os

curDir = os.getcwd()

launcher = Launcher({
    "SOFA_PATH" : "<path-to-sofa-build>/bin/runSofa",
    "SCENE_PATH" : os.path.join(curDir, "scene.py"),
    "RESULT_DIR_PATH" : os.path.join(curDir, "Res"),
    "VTK_DIR_PATH" : os.path.join(curDir, "Res", "vtk"),
    "MESH_STL" : os.path.join(curDir, "..", "mesh", "undeformed.stl"),
    "MESH_VKT" : os.path.join(curDir, "..", "mesh", "undeformed.vtk"),
    "N_IT" : 1000,
    "DT" : 0.1,
    "PRESSURE" : -200,
    "POISSON_RATIO" : 0.3,
    "YOUNG_MODULUS" : 15e5,
    "MASS" : 1e5,
    "METHOD" : "FEM",
})

launcher.cleanResDir()
launcher.addSubDir(launcher["VTK_DIR_PATH"])

launcher["MESH_DEFORMED"] = os.path.join(launcher["VTK_DIR_PATH"], "deformed_FEM.vtk")
launcher.run()

launcher.setParams({"METHOD":"Flexible", "MESH_DEFORMED":os.path.join(launcher["VTK_DIR_PATH"], "deformed_Flex.vtk")})
launcher.run()
