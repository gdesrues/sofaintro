# Run a simulation with different methods

The `PythonScriptController` component allows to call classes at runtime
```
rootNode.createObject("PythonScriptController", name="method", filename=__file__, classname=params["METHOD"], listening=1)
```

Here `deformed_FEM.vtk` will be the result of the classical FEM method while `deformed_Flex.vtk` will be obtained with the Flexible plugin approach
