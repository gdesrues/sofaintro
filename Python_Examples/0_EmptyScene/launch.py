from os.path import join
from os import getcwd
import sys
from sofatools import Launcher

curDir = getcwd()

launcher = Launcher({
    "SOFA_PATH" : "/user/gdesrues/home/sofa/v19.06/build-src-SofaGaetan-Release/bin/runSofa",
    "SCENE_PATH" : join(curDir, "scene.py"),
    "RESULT_DIR_PATH" : join(curDir, "Res"),
    "N_IT" : 0,
})

launcher.cleanResDir()
launcher.run()
