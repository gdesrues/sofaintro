import Sofa
import os
from json import load as loadJson
from argparse import ArgumentParser


class Scene(Sofa.PythonScriptController):
    def __init__(self, node, args):
        if len(args)!=1: raise SyntaxError("Please add the json file name, e.g. runSofa {} --argv jsonFileName.json".format(__file__))
        if not os.path.exists(args[0]): raise FileNotFoundError(args[0])
        global params
        params = loadJson(open(args[0],'r'))
        self.createGraph(node)

    def createGraph(self, rootNode):
        self.rootNode = rootNode
        rootNode.createObject('DefaultAnimationLoop')
        rootNode.createObject("RequiredPlugin", pluginName="SofaPython")


def createScene(rootNode):
    parser = ArgumentParser()
    parser.add_argument('--argv', action='store_true')
    _, args = parser.parse_known_args()
    Scene(rootNode, args)
