import Sofa
import os
import argparse
import json

class Scene(Sofa.PythonScriptController):
    def __init__(self, node, args):
        if len(args)!=1:
            raise SyntaxError("Please add the json file name, e.g. runSofa {} --argv jsonFileName.json".format(__file__))
        if not os.path.exists(args[0]):
            raise FileNotFoundError(args[0])
        global params
        params = json.load(open(args[0],'r'))

        self.createGraph(node)
        return None


    def createGraph(self, rootNode):
        # rootNode
        self.rootNode = rootNode
        rootNode.dt = params["DT"]
        rootNode.gravity = params["GRAVITY"]
        rootNode.createObject('VisualStyle', displayFlags='showVisual showWireframe showForceFields showBehavior')
        rootNode.createObject('DefaultAnimationLoop')
        rootNode.createObject('DefaultVisualManagerLoop')
        rootNode.createObject("RequiredPlugin", pluginName="Flexible SofaOpenglVisual SofaExporter SofaPython")
        rootNode.createObject('EulerImplicitSolver', rayleighStiffness='0.1', rayleighMass='0.1')
        rootNode.createObject('CGLinearSolver', iterations='100')


        # rootNode/Bar
        Bar = rootNode.createChild('Bar')
        self.Bar = Bar
        Bar.createObject('MeshVTKLoader', name='loader', filename=params["MESH_VKT"]) # 10m x 1m x 1m
        Bar.createObject('Mesh', name="mesh", src="@loader")
        Bar.createObject('MechanicalObject', name='dof', template="Vec3d")
        Bar.createObject('TetrahedronFEMForceField', name='FEM', method="small",
                        poissonRatio=params["POISSON_RATIO"],
                        youngModulus=params["YOUNG_MODULUS"])
        Bar.createObject('UniformMass', totalMass=params["MASS"])

        Bar.createObject('VTKExporter', filename=params["MESH_DEFORMED"], XMLformat=0, edges=0, triangles=0, tetras=1,
                        exportAtEnd=1, position="@dof.position", overwrite=1)


        # rootNode/Bar/DirichletBoundary
        DirichletBoundary = Bar.createChild('DirichletBoundary')
        self.DirichletBoundary = DirichletBoundary
        DirichletBoundary.createObject('BoxROI', box='0 0 0 0 1 1', position='@../mesh.position', template='Vec3d', drawSize="10", drawPoints="1")
        DirichletBoundary.createObject('RestShapeSpringsForceField', points='@[-1].indices', template='Vec3d', stiffness='1E10')


        # rootNode/Bar/PressureBoundary
        PressureBoundary = Bar.createChild('PressureBoundary')
        self.PressureBoundary = PressureBoundary
        PressureBoundary.createObject('TriangleSetTopologyContainer', triangles="@../loader.triangles")
        PressureBoundary.createObject('TriangleSetTopologyModifier')
        PressureBoundary.createObject('TriangleSetTopologyAlgorithms')
        PressureBoundary.createObject('TriangleSetGeometryAlgorithms')
        PressureBoundary.createObject('BoxROI', box='0 0 1 10 1 1', position='@../mesh.position', template='Vec3d', drawSize="5",
                                    drawTriangles='1', triangles="@../loader.triangles")
        PressureBoundary.createObject('TrianglePressureForceField', name="Pressure", triangleList="@[-1].triangleIndices",
                                    pressure="0 0 {}".format(params["PRESSURE"]))


        # rootNode/Bar/visual
        visual = Bar.createChild('visual')
        self.visual = visual
        visual.createObject('MeshSTLLoader', name='stlloader', filename=params["MESH_STL"])
        visual.createObject('OglModel', name='Visual', template='ExtVec3d', src='@stlloader', color='gray')
        visual.createObject('BarycentricMapping', template='Vec3d,ExtVec3d')

        return 0


def createScene(rootNode):
    parser = argparse.ArgumentParser()
    parser.add_argument('--argv', action='store_true')
    parsed, args = parser.parse_known_args()
    myScene = Scene(rootNode, args)
    return 0
