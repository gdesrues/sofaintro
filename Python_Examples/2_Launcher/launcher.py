from os.path import join
from os import getcwd
import sys
try:
    from sofatools import Launcher
except ModuleNotFoundError as e:
    print("\n[{}] {}".format(e.__class__.__name__, e))
    print("This scene uses a launcher defined in the sofatools python package.\nInstall it with 'pip install sofatools'.\n")
    sys.exit()

curDir = getcwd()

launcher = Launcher({
    "SOFA_PATH" : "<path-to-sofa-build>/bin/runSofa",
    "SCENE_PATH" : join(curDir, "scene.py"),
    "RESULT_DIR_PATH" : join(curDir, "Res"),
    "VTK_DIR_PATH" : join(curDir, "Res", "vtk"),
    "MESH_STL" : join(curDir, "..", "mesh", "undeformed.stl"),
    "MESH_VKT" : join(curDir, "..", "mesh", "undeformed.vtk"),
    "PRESSURE" : -200,
    "POISSON_RATIO" : 0.3,
    "YOUNG_MODULUS" : 15e5,
    "MASS" : 1e5,
    # "G_MODE" : "qglviewer", # default is batch
    "N_IT" : 1000,
    "DT" : 0.1,
})

launcher["MESH_DEFORMED"] = join(launcher["VTK_DIR_PATH"], "deformed.vtk")

# launcher.setResDir() # Create dir as Res_i (new directory for each simulation)
launcher.cleanResDir() # Create dir Res (results erased at each simulation)
launcher.addSubDir(launcher["VTK_DIR_PATH"])

launcher.run()
