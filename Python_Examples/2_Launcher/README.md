# Launch the scene with parameters

To use the launcher, you need to install `sofatools`, visit [https://pypi.org/project/sofatools/](https://pypi.org/project/sofatools/).

See [the documentation](https://sofatools.readthedocs.io/en/latest/).

You need to set your `SOFA_PATH` (in `launcher.py`) to run the executable runSofa, from your build of Sofa-Framework. Then, run the scene with
```
python launcher.py
```

- It will create the results directory `RESULT_DIR_PATH` with the output files.
- Deformed mesh will be in `VTK_DIR_PATH`
- You can modify the mode of execution of runSofa (Gui/Shell) with `G_MODE`




## Virtual Environment
See the [FAQ of the cluster](https://wiki.inria.fr/ClustersSophia/FAQ_new_config#How_can_i_install_a_python_package_with_pip_.3F)

```
module load conda/5.0.1-python3.6

# create and use a conda virtual environment
conda create --name virt_conda python=3.6
source activate virt_conda

# install a package in virtual environment
conda install package

# leave the conda virtual environment
source deactivate virt_conda
```
